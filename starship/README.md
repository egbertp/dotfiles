# Uninstall Oh My ZSH


```sh
uninstall_oh_my_zsh
Are you sure you want to remove Oh My Zsh? [y/N] y
Removing ~/.oh-my-zsh
Found ~/.zshrc -- Renaming to /Users/egbert/.zshrc.omz-uninstalled-2023-05-06_12-01-39
Looking for original zsh config...
Found /Users/egbert/.zshrc.pre-oh-my-zsh -- Restoring to ~/.zshrc
Your original zsh config was restored.
Thanks for trying out Oh My Zsh. It's been uninstalled.
Don't forget to restart your terminal!
```
