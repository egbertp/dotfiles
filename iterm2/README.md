# Referendes

- [iTerm2 + Oh My Zsh + Solarized color scheme + Source Code Pro ](https://gist.github.com/kevin-smets/8568070)

- [Github - nerd-fonts 50+ patched fonts](https://github.com/ryanoasis/nerd-fonts/)

- [Source Code Pro](https://github.com/adobe-fonts/source-code-pro/blob/release/OTF/SourceCodePro-Regular.otf)
- [Meslo LGS Nerd Font]()
- [Solarized - Precision colors for machines and people](https://ethanschoonover.com/solarized/)

- [VS Code documentation - User and Workspace Settings](https://code.visualstudio.com/docs/getstarted/settings)

- [Starship - The minimal, blazing-fast, and infinitely customizable prompt for any shell! ](https://starship.rs/)

- [NerdFonts cheat sheet](https://www.nerdfonts.com/cheat-sheet)
