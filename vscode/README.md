# VSCode

## Installed Extensions

```sh
$ code --list-extensions | xargs -L 1 echo code --install-extension
```

```
code --install-extension 42Crunch.vscode-openapi
code --install-extension donjayamanne.githistory
code --install-extension eamodio.gitlens
code --install-extension esbenp.prettier-vscode
code --install-extension fabiospampinato.vscode-diff
code --install-extension golang.go
code --install-extension hashicorp.terraform
code --install-extension humao.rest-client
code --install-extension janisdd.vscode-edit-csv
code --install-extension marp-team.marp-vscode
code --install-extension mikeburgh.xml-format
code --install-extension mohsen1.prettify-json
code --install-extension ms-azure-devops.azure-pipelines
code --install-extension ms-azuretools.vscode-docker
code --install-extension ms-dotnettools.csharp
code --install-extension ms-python.python
code --install-extension ms-python.vscode-pylance
code --install-extension ms-toolsai.jupyter
code --install-extension ms-toolsai.jupyter-keymap
code --install-extension ms-toolsai.jupyter-renderers
code --install-extension ms-vscode-remote.remote-containers
code --install-extension ms-vscode.azure-account
code --install-extension ms-vscode.hexeditor
code --install-extension ms-vscode.powershell
code --install-extension Oracle.oracledevtools
code --install-extension quicktype.quicktype
code --install-extension rebornix.ruby
code --install-extension redhat.fabric8-analytics
code --install-extension redhat.java
code --install-extension redhat.vscode-commons
code --install-extension redhat.vscode-yaml
code --install-extension rust-lang.rust
code --install-extension serayuzgur.crates
code --install-extension TomAustin.azure-devops-yaml-pipeline-validator
code --install-extension VisualStudioExptTeam.vscodeintellicode
code --install-extension vscjava.vscode-java-debug
code --install-extension vscjava.vscode-java-dependency
code --install-extension vscjava.vscode-java-pack
code --install-extension vscjava.vscode-java-test
code --install-extension vscjava.vscode-maven
code --install-extension vscode-icons-team.vscode-icons
code --install-extension vscodevim.vim
code --install-extension waderyan.gitblame
code --install-extension wholroyd.jinja
code --install-extension wingrunr21.vscode-ruby
code --install-extension zhuangtongfa.material-theme
code --install-extension zxh404.vscode-proto3
```

https://zellwk.com/blog/prettier-disable-languages/
https://code.visualstudio.com/docs/getstarted/settings
https://github.com/prettier/prettier-vscode#default-formatter
