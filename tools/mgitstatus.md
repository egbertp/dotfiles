# mgitstatus

Repository: https://github.com/fboender/multi-git-status

```sh
$ sudo cp mgitstatus /usr/local/bin/.
```

```sh
~/Development/EPIC $ mgitstatus

./projects/tfc-getting-started: Uncommitted changes Untracked files
./projects/egbert.space: Untracked files
./projects/egbert.space.infra: Uncommitted changes Untracked files
./projects/egbert.space.plenti: Uncommitted changes Untracked files
./projects/docker-gollum: ok
./knowledge-base: Needs push (master)
```
